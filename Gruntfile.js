module.exports = function(grunt) {

    grunt.initConfig({
        
        sass: {
            dev: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'css/main.css': 'sass/main.scss'
                }
            },
            dist: {}
        },

        concat_css: {
            dev: {
                src: ['css/main.css', 'bower_components/toaster/toaster.css'],
                dest: 'css/styles.css'
            },
            dist: {}
        },

        concat: {
            concat_bower: {
                src: [
                    'bower_components/angular/angular.js',
                    'bower_components/angular-route/angular-route.js',
                    'bower_components/angular-animate/angular-animate.js',
                    'bower_components/angular-ui-bootstrap/dist/ui-bootstrap-custom-tpls-0.12.0.js',
                    'bower_components/firebase/firebase.js',
                    'bower_components/angularfire/dist/angularfire.js',
                    'bower_components/firebase-simple-login/firebase-simple-login.js',
                    'bower_components/toaster/toaster.js',
                    'bower_components/linqjs/linq.js'
                ],
                dest: 'js/libs.js'
            },
            dev_app: {
                src: ['js/app/app.js', 'js/app/**/*.js'],
                dest: 'js/all.js'
            },
            dist_app: {}
        },

        watch: {
            css: {
                files: ['sass/*'],
                tasks: ['sass:dev', 'concat_css:dev']
            },
            js: {
                files: ['js/app/app.js', 'js/app/**/*.js'],
                tasks: ['concat:dev_app']
            }
        },

        copy: {
            fonts: {
                expand: true,
                cwd: 'bower_components/bootstrap-sass-official/assets/fonts/',
                src: ['**'],
                dest: 'fonts/'
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-concat-css');

    grunt.registerTask('default', ['sass:dev', 'concat_css:dev', 'concat:dev_app', 'watch']);
    grunt.registerTask('concat_bower', ['concat:concat_bower', 'copy:fonts']);
};