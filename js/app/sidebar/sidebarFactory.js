(function (angular, E) {
    'use strict';

    angular.module('todo').factory('todo.sidebarFactory', sidebar);

    sidebar.$inject = ['$firebase', 'DB', 'todo.flow'];

    function sidebar($firebase, DB, flow) {

        var ref = new Firebase(DB.ref + DB.paths.categories);
        var sync = $firebase(ref);
        var categories = sync.$asArray();

        categories.$loaded().then(function() {

            flow.activeCategory = 'all';
        });

        function addCategory(name) {

            var newOrder = E.from(categories).max(function(i) { return parseInt(i.order); }) + 1;
            return categories.$add({ name: name, order: newOrder });
        }

        function deleteCategory(item) {

            return categories.$remove(item);
        }

        function saveCategory(item) {

            return categories.$save(item);
        }

       	return {
            categories: categories,
            loaded: categories.$loaded,
            addCategory: addCategory,
            deleteCategory: deleteCategory,
            saveCategory: saveCategory
    	}
    }
})(angular, Enumerable);