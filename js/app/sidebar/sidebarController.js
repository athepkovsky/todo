(function (angular) {
    'use strict';

    angular.module('todo').controller('todo.sidebarController', sidebarController);

    sidebarController.$inject = ['$scope', '$rootScope', '$modal', 'todo.sidebarFactory', 'todo.flow', 'toaster'];

    function sidebarController($scope, $rootScope, $modal, sidebarFactory, flow, toaster) {

        var self = this;
        self.flow = flow;
        self.list = sidebarFactory.categories;

        $rootScope.$broadcast('Server.StartRequest');

        sidebarFactory.loaded().then(function() {

            $rootScope.$broadcast('Server.FinishRequest');
        });

        self.add = function() {

            $rootScope.$broadcast('Server.StartRequest');
            sidebarFactory.addCategory(self.addName).then(function() {

                toaster.pop('success', 'Category Added', 'Сategory <code>' + self.addName + '</code> has been added successfully!', 5000, 'trustedHtml');
                self.addName = '';
                $rootScope.$broadcast('Server.FinishRequest');
            });
        };

        self.delete = function (category) {

            var modalInstance = $modal.open({
                
                templateUrl: 'js/app/sidebar/deleteConfirmationModal.template.html',
                controller: 'todo.sidebar.deleteConfirmationModalController as modalController',
                resolve: { nameDeleteCategory: function () { return category.name; }}
            });

            modalInstance.result.then(function () {
                
                $rootScope.$broadcast('Server.StartRequest');

                sidebarFactory.deleteCategory(category).then(function() {

                    toaster.pop('success', 'Category Deleted', 'Сategory <code>' + category.name + '</code> has been deleted successfully!', 5000, 'trustedHtml');
                    $rootScope.$broadcast('Server.FinishRequest');
                });
            });
        };

        self.edit = function(category) {

            angular.forEach(self.list, function(i) { delete i.clone; });
            category.clone = angular.copy(category);
        };

        self.cancelEdit = function(category) {

            delete category.clone;
        };

        self.save = function(category) {

            $rootScope.$broadcast('Server.StartRequest');

            category.name = category.clone.name;
            delete category.clone;

            sidebarFactory.saveCategory(category).then(function() {

                toaster.pop('success', 'Category Saved', 'Сategory <code>' + category.name + '</code> has been saved successfully!', 5000, 'trustedHtml');
                $rootScope.$broadcast('Server.FinishRequest');
            });
        };


    }
})(angular);