(function (angular) {
    'use strict';

    angular.module('todo').controller('todo.sidebar.deleteConfirmationModalController', deleteConfirmationModalController);

    deleteConfirmationModalController.$inject = ['nameDeleteCategory'];

    function deleteConfirmationModalController(nameDeleteCategory) {

        var self = this;
        self.nameDeleteCategory = nameDeleteCategory;
    }
})(angular);