(function (angular) {
    'use strict';

    angular.module('todo').factory('todo.todoFactory', todo);

    todo.$inject = ['$firebase', 'DB', 'todo.flow'];

    function todo($firebase, DB, flow) {

        var ref = new Firebase(DB.ref + DB.paths.todos + flow.activeCategory);
        var sync = $firebase(ref);
        var todos = sync.$asArray();

        todos.$loaded().then(function() {
            
        });

       	return {
            todos: todos,
            loaded: todos.$loaded
    	}
    }
})(angular);