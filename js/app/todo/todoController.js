(function (angular) {
    'use strict';

    angular.module('todo').controller('todo.todoController', todoController);

    todoController.$inject = ['$rootScope', 'todo.todoFactory'];

    function todoController($rootScope, todoFactory) {
    	
    	var self = this;
        self.list = todoFactory.todos;
    }
})(angular);