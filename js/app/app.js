;(function (angular) {
    'use strict';

    var module = angular.module('todo', ['ngRoute', 'ngAnimate', 'firebase', 'toaster', 'ui.bootstrap']);

    config.$inject = ['$routeProvider', '$locationProvider'];
    checkLogin.$inject = ['$rootScope', '$location', '$q', '$timeout', 'todo.loginFactory'];

    module.config(config);

    function config($routeProvider, $locationProvider) {

        var loginRoute = {
            templateUrl: 'js/app/login/login.template.html',
            controller: 'todo.loginController as login'
        };

        var todoRoute = {
            templateUrl: 'js/app/todo/todo.template.html',
            controller: 'todo.todoController',
            resolve: [checkLogin]
        };

        $routeProvider
            .when('/', todoRoute)
            .when('/todo/', todoRoute)
            .when('/login/', loginRoute)
            .otherwise('/');

        $locationProvider.html5Mode(true);
    }

    module.constant('DB', {
    	ref: 'http://todozippovich.firebaseio.com/',
    	paths: {
    		categories: 'categories/'
    	}
    });

    function checkLogin($rootScope, $location, $q, $timeout, loginFactory) {

        $rootScope.$broadcast('Server.StartRequest');

        return loginFactory.checkLogin().then(function() {

            $rootScope.$broadcast('Server.FinishRequest');
            return $q.when();
        }, function() {

            $rootScope.$broadcast('Server.FinishRequest');
            $location.path() !== '/login/' && $location.path('/login/');
            return $timeout(function() { return $q.when() }, 200);
        });
    }
})(angular);