(function (angular, E) {
        'use script';

        angular.module('todo').factory('todo.arrayUtils', arrayUtils);

        function arrayUtils() {

                function copyArray(source, destination, transform) {

                        destination.length = 0;
                        E.From(source).Select(function (i) { return (transform && transform(i)) || i; }).ForEach(function (i) { destination.push(i); });
                }

                function mergeArray(source, destination, equalityS, equalityD, transform) {

                        var items = E.From(source).GroupJoin(destination, equalityS, equalityD, function (s, d) { return { source: s, destination: d.FirstOrDefault(null) }; }).ToArray();
                        copyArray(items, destination, transform);
                }

                function deleteFromArray(array, selector) {

                        if (!array || !selector)
                                return;

                        if(!angular.isFunction(selector)) {

                                var index = array.indexOf(selector);

                                if (index != -1)
                                        array.splice(index, 1);

                                return;
                        }

                        var elemToDelete = [];

                        E.From(array).ForEach(function (i) { if (selector(i)) { elemToDelete.push(i); } });

                        E.From(elemToDelete).ForEach(function (i) { var index = array.indexOf(i); array.splice(index, 1); });
                }

                return {

                        copyArray: copyArray,
                        mergeArray: mergeArray,
                        deleteFromArray: deleteFromArray
                };
        }
}) (angular, Enumerable);