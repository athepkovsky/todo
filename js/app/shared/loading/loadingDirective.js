(function (angular) {
    'use strict';

    var module = angular.module('todo');

    module.directive('todoLoading', [function () {

        return {
            replace: true,
            scope: true,
            templateUrl: 'js/app/shared/loading/loading.template.html',
            restrict: 'EA',
            controller: ['$scope', function ($scope) {

                var model = $scope.model = {
                    loading: false
                };

                $scope.$on('Server.StartRequest', function () { model.loading = true; });
                $scope.$on('Server.FinishRequest', function () { model.loading = false; });
            }]
        };
    }]);
})(angular);