(function (angular, Firebase) {
    'use strict';

    angular.module('todo').factory('todo.loginFactory', login);

    login.$inject = ['$q', '$firebaseAuth', 'DB'];

    function login($q, $firebaseAuth, DB) {

    	var ref = new Firebase(DB.ref);
    	var deferred = $q.defer();
        var loginObj = $firebaseAuth(ref);

    	function checkLogin() {

            var authData = loginObj.$getAuth();

            if (authData) {
                deferred.resolve(authData.uid);
            } else {
                deferred.reject();
            }

            return deferred.promise;
        }

    	function doLogin(email, password) {
            
            deferred = $q.defer();
            loginObj.$authWithPassword({ email: email, password: password}).then(
                function (user) {
                    deferred.resolve(user);
                },
                function (error) {
                    deferred.reject(error);
                }
            );
            return deferred.promise;
    	}

    	return {
    		checkLogin: checkLogin,
    		doLogin: doLogin
    	};
    }
})(angular, Firebase);