(function (angular) {
    'use strict';

    angular.module('todo').controller('todo.loginController', loginController);

    loginController.$inject = ['$location', '$rootScope', 'todo.loginFactory', 'toaster'];

    function loginController($location, $rootScope, loginFactory, toaster) {

        var self = this;

        self.doLogin = function() {

            $rootScope.$broadcast('Server.StartRequest');

            loginFactory.doLogin(self.email, self.password).then(function () {

                toaster.pop('success', 'Login', 'User successfully logged on!');
                $location.path('/todo/');
                $rootScope.$broadcast('Server.FinishRequest');
            }, function(error) {

                toaster.pop('error', 'Login', error.message);
                $rootScope.$broadcast('Server.FinishRequest');
            });
        };
    }
})(angular);